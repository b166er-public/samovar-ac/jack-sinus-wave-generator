#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <signal.h>
#include<pthread.h>

#include <jack/jack.h>


typedef struct jack_client_session {
    jack_client_t* client_handler;
    jack_port_t* pcm_output_port;
} jack_client_session_t;

static jack_client_session_t* gbl_jack_session_ptr = NULL;
static float* gbl_sinus_phase_buffer_ptr = NULL;
static int gbl_sinus_buffer_size = 256;
static pthread_mutex_t gbl_kill_mutex;

int
callback_process(
    jack_nframes_t nframes, 
    void *arg
) {
    static int cycle = 0;
    static int last_phase_idx = 0;
    printf("Process cycle %d ... \n", cycle++);
    
    // Get session pointer
    jack_client_session_t* session_ptr = (jack_client_session_t*) arg;
    
    // Get buffer for current output port
    jack_default_audio_sample_t* buffer_out = jack_port_get_buffer(
        session_ptr->pcm_output_port, 
        nframes
    );
    
    int phase_start = last_phase_idx;
    last_phase_idx = (last_phase_idx + nframes) % gbl_sinus_buffer_size;
    
    
    for(int f=0; f < nframes; f++) {
        int phase_idx = (phase_start + f) % gbl_sinus_buffer_size;
        buffer_out[f] = gbl_sinus_phase_buffer_ptr[phase_idx];
    }
    
    printf("Rendered %d frames!\n", nframes);
    
    return 0;
}

void
callback_jack_shutdown(
    void *arg
)
{
    exit(1);
}


jack_client_session_t*
create_jack_client() {
    jack_client_session_t* result = (jack_client_session_t*) malloc(sizeof(jack_client_session_t));
    result->client_handler = jack_client_open(
        "sinus_generator", /* client name*/
        JackNullOption, /* options */
        NULL, /* status */
        NULL /* server name */
    );
    
    if (result->client_handler == NULL) {
        fprintf(stderr, "Could not open a JACK client!");
        exit (1);
    } else {
        jack_set_process_callback(
            result->client_handler, 
            callback_process, 
            result
        );
        jack_on_shutdown(
            result->client_handler, 
            callback_jack_shutdown, 
            0
        );
        
        result->pcm_output_port = jack_port_register(
            result->client_handler, 
            "output",
            JACK_DEFAULT_AUDIO_TYPE,
            JackPortIsOutput, 
            0
        );
        
        if (jack_activate(result->client_handler)) {
            fprintf (stderr, "JACK Client starting ... FAILED!");
            exit(1);
        } else {
            printf("JACK Client starting... OK\n");
        }

    }
    
    return result;
}

void
destroy_jack_client(
    jack_client_session_t* session
) {
    jack_client_close(session->client_handler);
    printf("JACK Client stopping... OK\n");
}

float*
render_sinus_phase_buffer(
    int period_in_frames
) {
    float* buffer = malloc(sizeof(float) * period_in_frames);
    for(int i=0; i < period_in_frames; i++) {
        buffer[i] = sin(2 * M_PI / ((float) period_in_frames) * i);
    }
    return buffer;
}


void
stop_generator(int signal) {
    printf("You requested me to die!\n");
    pthread_mutex_unlock(&gbl_kill_mutex);
}


int 
main() {
    // Register signal handler to correctly react on Ctrl+C
    signal(SIGINT, stop_generator);
    
    // Create mutex to wait until generator is requested to die
    pthread_mutex_init(&gbl_kill_mutex, NULL);
    pthread_mutex_lock(&gbl_kill_mutex);
    
    gbl_jack_session_ptr = create_jack_client();
    gbl_sinus_phase_buffer_ptr = render_sinus_phase_buffer(gbl_sinus_buffer_size);
    
    // Wait until the user wants me to die
    pthread_mutex_lock(&gbl_kill_mutex);
    
    destroy_jack_client(gbl_jack_session_ptr);
    return 0;
}