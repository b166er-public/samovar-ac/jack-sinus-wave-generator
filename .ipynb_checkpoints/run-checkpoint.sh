#!/bin/bash

# Clear & Create the target folder
rm -rf target
mkdir target

# Compile the generator
gcc -g -c ./src/program.c -o ./target/program.o

# Link the generator
gcc -g -o ./target/program -ljack -lm ./target/program.o

# Make generator executable
chmod +x ./target/program

# Execute the program
./target/program